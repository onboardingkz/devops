# Getting started
Only things you need here are Docker and DockerCompose.
Instruction about installation is [here](https://docs.docker.com/engine/install/).

## Linux instruction
If you are on Linux just execute `deploy.sh` shellscript.
```$ sh deploy.sh```

## Windows instruction
If you are on Windows, a bad new is you must've been doing everything manually. Just kidding, :D
But here are your steps:
- Clone service repositories into `services` folder:
```
$ git clone git clone git@gitlab.com:onboardingkz/backend/api.git ./services/api
$ git clone git@gitlab.com:onboardingkz/backend/socket.git ./services/socket
$ git clone git@gitlab.com:onboardingkz/backend/cdn-service.git ./services/cdn-service
$ git clone git@gitlab.com:onboardingkz/frontend/frontend.git ./services/frontend

```
> IMPORTANT! Don't change anything above. Commands will clone your repos into `services` folder. Folder names are also important.

- Build your images and run your containers:
```
$ docker-compose up -d
```
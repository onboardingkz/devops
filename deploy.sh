#!/bin/sh
[ ! -d "./services/api/" ] && git clone git@gitlab.com:onboardingkz/backend/api.git ./services/api
[ ! -d "./services/socket/" ] && git clone git@gitlab.com:onboardingkz/backend/socket.git ./services/socket
[ ! -d "./services/cdn-service/" ] && git clone git@gitlab.com:onboardingkz/backend/cdn-service.git ./services/cdn-service
[ ! -d "./services/frontend/" ] && git clone git@gitlab.com:onboardingkz/frontend/frontend.git ./services/frontend

docker-compose up -d